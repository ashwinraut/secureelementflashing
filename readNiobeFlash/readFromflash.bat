@echo off
echo.
echo ************************************************
echo "SIMBAND script for Flashing axf secure element"
echo ************************************************
echo.
echo "Killing Old tasks only if they are present, May get a warning if they are not present"
tskill redlinkserv
tskill arm-none-eabi_gdb*
tskill crt_emu_*
echo ----------------------------------------------------------
echo.
cscript /nologo listusb.vbs | sort
echo.
echo Loading Firmware into LPC LINK2 and trying to boot it...
echo.
setlocal enabledelayedexpansion
set BootHome=C:\nxp\LPCXpresso_7.5.0_254\lpcxpresso\bin\
set DeviceName=LPC-Link2
set BootImageWild=LPC432x*.hdr 
set DfuLog=%temp%\dfu-util.log

call :getDfuVidPid
if "%DfuVidPid%" == "" goto :NoDfus
call :getBootImage
if "%BootImage%" == "" goto :NoBootImage

:DfuApp
for %%i in (%BootImage%) do set ShortBootImage=%%~nxi
echo Booting %DeviceName% with %ShortBootImage%
set boot_options=-d 0x1fc9:c -c 0 -i 0 -t 2048 -R -D %BootImage%

%BootHome%\dfu-util %boot_options% >NUL 2>%DfuLog%
if %errorlevel% equ 0 (
  echo %DeviceName% booted
  echo ----------------------------------------------------------
  echo Wait for boot timeout to expire , DONOT press the key to continue
  echo.
  TIMEOUT /t 5
  echo ----------------------------------------------------------
  echo Loading secure element firmware file into Niobe...
  echo.
  %BootHome%\crt_emu_cm_redlink -flash-load-exec "%1" -g -2  -vendor=NXP -pLPC54102J512 -load-base=0x0  -flash-driver=%BootHome%\Flash\LPC5410x_512K.cfx
  echo.
  echo Check for No Error above
  echo.
  echo ----------------------------------------------------------
  echo Wait for Niobe to flash secure element and timeout to expire...
  echo.
  TIMEOUT /t 30
  echo ----------------------------------------------------------
  echo Killing Old tasks only if they are present, May get a warning if they are not present
  echo.
  tskill redlinkserv
  tskill arm-none-eabi_gdb*
  tskill crt_emu_*
  echo ----------------------------------------------------------
  echo Executing the Script to read from Niobe Flash...
  echo.
  C:\nxp\LPCXpresso_7.5.0_254\lpcxpresso\bin\redlinkserv -script=flashread.scp
  echo ----------------------------------------------------------
  echo Loading secondary bootloader file into Niobe...
  echo.
  %BootHome%\crt_emu_cm_redlink -flash-load-exec "secondary_loader.axf" -g -2  -vendor=NXP -pLPC54102J512 -load-base=0x0  -flash-driver=%BootHome%\Flash\LPC5410x_512K.cfx
  echo ----------------------------------------------------------
) else (
  echo ERROR %DeviceName% boot failed:
  type %DfuLog%
)
goto :end

:Usage
echo Usage: %0 <firmware.axf file >
goto :end

:end
endlocal
goto :eof

:NoDfus
echo.
echo ERROR Nothing to boot , Unplug/plug LPC-LINK2
goto :end

:NoBootImage
echo ERROR No boot image found
goto :end

:getDfuVidPid
for /f "skip=6 tokens=3" %%a in ('%BootHome%dfu-util -l') do (
  set DfuVidPid=%%a
)
:eof

:getBootImage
for /r %BootHome% %%f in (%BootImageWild%) do (
  set BootImage=%%f
)
:eof
