Option Explicit
Dim oWMISrv, collDvcs, collUSBDvcs, iUSBDvc , iDvc, sDvcID, sPID, sVID , lpclinkPresent

Function ReplaceX(ByVal sValue, ByVal sPattern, ByVal sNValue)
Dim oReg : Set oReg = New RegExp
oReg.Pattern = sPattern
ReplaceX = oReg.Replace(sValue, sNValue)
Set oReg = Nothing
End Function
lpclinkPresent=0
Set oWMISrv = GetObject("winmgmts:\\.\root\cimv2")
Set collDvcs = oWMISrv.ExecQuery("Select * From Win32_PnPEntity")

For Each iDvc In collDvcs
  If InStr(iDvc.PNPDeviceID, "VID_") Then ' Except keychain drives
    sDvcID = ReplaceX(iDvc.PNPDeviceID, ".*""(.*)""", "$1")
    sPID = ReplaceX(sDvcID, ".*PID_([^\\\&\+]*).*", "$1")
    sVID = ReplaceX(sDvcID, ".*VID_([^&\+]*).*", "$1")
    
    'Wscript.Echo "VID: " & sVID & " PID: " & sPID & " ("& iDvc.Description & ") "
    If sVID = "21BD" Then
         lpclinkPresent = "2"
    ElseIf sVID = "1FC9" Then
         lpclinkPresent = "1"
    End If 
  End If
Next

Set collDvcs = Nothing
Set oWMISrv = Nothing

If lpclinkPresent = "1" Then
    Wscript.Echo "LPC-LINK2 Doesnt have firmware, we are good to go"    
ElseIf lpclinkPresent = "2" Then
    MsgBox "LPC-LINK2 has firmware already in it , Please disconnect and reconnect LPC-LINK2 USB, Then rerun the script"
Else
    MsgBox "LPC-LINK2 not connected to the computer"
End If 

