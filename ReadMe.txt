1. Install LPC expresso 7.4.0_229 exeutable given in this folder.
2. Activate the IDE
3. Make sure you have LPC link2 , and Apollo connected properly hardware wise.
4. Plug the LPC link2 usb to windows computer.
5. wait 2-3 seconds then Using windows command prompt navigate to the readNiobeFlash directory and 
 execute following:
 readFromflash.bat niobesecureelementfirmware.axf 
 Make sure the paths are correct for both above commands
6. repeat point 4- 6 for each different fimrware.
If flashing status of secure element fails repeatedly then keep the apollo aside and the axf file also.


For any issues contact Arash Shakeri-Far (a.shakeri@ssi.samsung.com) or Ashwin Raut(ashwin.raut@ssi.samsung.com)